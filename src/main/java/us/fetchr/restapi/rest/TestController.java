package us.fetchr.restapi.rest;

import org.json.simple.JSONObject;
import org.postgresql.jdbc.PgArray;
import org.postgresql.util.PGobject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.json.simple.parser.JSONParser;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@RestController
public class TestController {

    /*
        https://www.jianshu.com/p/c8a01ae9f779
        https://github.com/alibaba/druid
     */

    @Autowired
    JdbcTemplate jdbcTemplate;

    @RequestMapping(value = {"/", "/hello"})
    public String sayHello(@RequestParam(value = "name", defaultValue = "World") String name) {
        return "Hello: " + name + "! ";
    }

    @RequestMapping(value = "/email_to/{id}", method = RequestMethod.GET)
    public List<String> email(@PathVariable(value = "id") Integer id) throws Exception {
        List<String> res = getEmailToAddressById(id);
        return res;
    }

    public List<String> getEmailToAddressById(Integer id) throws Exception {
        String sql = "select email_to from output_configuration_email where id = ? ";
        List<PgArray> res = jdbcTemplate.queryForList(sql, new Object[] {id}, PgArray.class);
        String[] emails = (String[]) res.get(0).getArray();
        return Arrays.asList(emails);
    }

    @RequestMapping(value = "/webhook_headers/{id}", method = RequestMethod.GET)
    public Object webhook(@PathVariable(value = "id") Integer id) throws Exception {
        Object res = getHeadersById(id);
        return res;
    }

    public List<String> getHeadersById(Integer id) throws Exception {
        String sql = "select headers_dictionary from output_configuration_webhook where id = ? ";
        List<PGobject> res = jdbcTemplate.queryForList(sql, new Object[] {id}, PGobject.class);

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(res.get(0).getValue());

        List<String> headers = new LinkedList<String>();
        for (Object key : jsonObject.keySet()) {
            headers.add((String) jsonObject.get(key));
        }

        return headers;
    }


}
